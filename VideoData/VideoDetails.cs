﻿using System;

namespace VideoData
{
    public class VideoDetails
    {
        public int VideoId;
        public DateTime PublishDate;
        public string Url;
    }
}