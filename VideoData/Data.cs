﻿using System;
using System.Collections.Generic;
using Microsoft.Win32.SafeHandles;

namespace VideoData
{
    public class Data
    {
        private List<Video> _video = new List<Video>();
        private List<VideoDetails> _videoDetails = new List<VideoDetails>();
        private List<Creator> _creators = new List<Creator>();
        private List<Rank> _rank = new List<Rank>();
        private List<Comments> _comments = new List<Comments>();

        /// <summary>
        /// Public Constructor for Video Data - Initialized Dataset
        /// </summary>
        public Data()
        {
            var videoIdCounter = 120;
            var creatorIdCounter = 50;

            //Initialize Data

            // Add Casey - 'Success'
            _video.Add(new Video(){Creator = "Casey Neistat", Id = videoIdCounter, Title = "‘Success’ on YouTube Still Means a Life of Poverty" });
            _creators.Add(new Creator(){Id = creatorIdCounter, Name = "Casey Neistat", Followers = 9000000, ChannelUrl = "https://www.youtube.com/channel/UCtinbF-Q-fVthA0qrFQTgXQ" });
            _videoDetails.Add(new VideoDetails(){VideoId = videoIdCounter, Url = "https://www.youtube.com/watch?v=xyVdZrL3Sbo", PublishDate = new DateTime(2018, 3, 1)});
            _rank.Add(new Rank(){VideoId = videoIdCounter, Views = 85464, Likes = 10000});
            _comments.Add(new Comments(){VideoId = videoIdCounter, CommentsEnabled = true, CommentCount = 1442});
            videoIdCounter++;
            creatorIdCounter++;

            // Add Kyle - Vibranium
            _video.Add(new Video(){Id = videoIdCounter, Creator = "Because Science", Title = "What Would Really Happen if a Vibranium Meteor Hit Earth? | Because Science w/ Kyle Hill" });
            _creators.Add(new Creator(){Id = creatorIdCounter, Name = "Because Science", Followers = 186000, ChannelUrl = "https://www.youtube.com/channel/UCvG04Y09q0HExnIjdgaqcDQ" });
            _videoDetails.Add(new VideoDetails(){VideoId = videoIdCounter, Url = "https://www.youtube.com/watch?v=n8dwN4-U1zE", PublishDate = new DateTime(2018, 3, 1)});
            _rank.Add(new Rank(){VideoId = videoIdCounter, Likes = 4000, Views = 43940});
            _comments.Add(new Comments(){VideoId = videoIdCounter, CommentsEnabled = true, CommentCount = 768});
            videoIdCounter++;
            creatorIdCounter++;

            // Add Devin SuperTramp - Worst Things
            _video.Add(new Video() { Id = videoIdCounter, Creator = "devingraham", Title = "Worst Thing to Happen to Us in the Last Two Years! The Real Story!" });
            _creators.Add(new Creator() { Id = creatorIdCounter, Name = "devingraham", Followers = 902000, ChannelUrl = "https://www.youtube.com/channel/UCzofNVHFCdD_4Jxs5dVqtAA" });
            _videoDetails.Add(new VideoDetails() { VideoId = videoIdCounter, Url = "https://www.youtube.com/watch?v=BdyIVZ7jk8I&t=0s", PublishDate = new DateTime(2018, 2, 28) });
            _rank.Add(new Rank() { VideoId = videoIdCounter, Likes = 391, Views = 10281 });
            _comments.Add(new Comments() { VideoId = videoIdCounter, CommentsEnabled = true, CommentCount = 44 });
            videoIdCounter++;
            creatorIdCounter++;

            // Add Shonduras - Lemon Challenge
            _video.Add(new Video() { Id = videoIdCounter, Creator = "Shonduras", Title = "Challenge accepted, Peter McKinnon." });
            _creators.Add(new Creator() { Id = creatorIdCounter, Name = "Shonduras", Followers = 1100000, ChannelUrl = "https://www.youtube.com/channel/UCoK5NOxkZBLfI_5eqf8Es4Q" });
            _videoDetails.Add(new VideoDetails() { VideoId = videoIdCounter, Url = "https://www.youtube.com/watch?v=hHkf5T5rfO0&t=0s", PublishDate = new DateTime(2018, 2, 28) });
            _rank.Add(new Rank() { VideoId = videoIdCounter, Likes = 2000, Views = 33831 });
            _comments.Add(new Comments() { VideoId = videoIdCounter, CommentsEnabled = true, CommentCount = 287 });
            videoIdCounter++;
            creatorIdCounter++;

            // Add PM - Lemon Challenge
            _video.Add(new Video() { Id = videoIdCounter, Creator = "Peter McKinnon", Title = "Challenge accepted, Casey." });
            _creators.Add(new Creator() { Id = creatorIdCounter, Name = "Peter McKinnon", Followers = 1700000, ChannelUrl = "https://www.youtube.com/channel/UC3DkFux8Iv-aYnTRWzwaiBA" });
            _videoDetails.Add(new VideoDetails() { VideoId = videoIdCounter, Url = "https://www.youtube.com/watch?v=b3ofgTrsXPI&t=0s", PublishDate = new DateTime(2018, 2, 27) });
            _rank.Add(new Rank() { VideoId = videoIdCounter, Likes = 25000, Views = 333968 });
            _comments.Add(new Comments() { VideoId = videoIdCounter, CommentsEnabled = true, CommentCount = 2654 });

        }

        /// <summary>
        /// Get aLl Videos
        /// </summary>
        /// <returns></returns>
        public List<Video> GetVideos()
        {
            return _video;
        }

        /// <summary>
        /// Get a specific Video by Id
        /// </summary>
        /// <param name="videoId"></param>
        /// <returns></returns>
        public Video GetVideo(int videoId)
        {
            Video result = null;

            //TODO: Use Linq
            foreach (var video in _video)
            {
                if (video.Id == videoId)
                    result = video;
            }

            return result;
        }

        /// <summary>
        /// Get a specific Video by Video Name (Case Sensitive)
        /// </summary>
        /// <param name="videoName"></param>
        /// <returns></returns>
        public Video GetVideo(string videoName)
        {

            return null;
        }

        /// <summary>
        /// Get Creator by Creator Id
        /// </summary>
        /// <param name="creatorId"></param>
        /// <returns></returns>
        public Creator GetCreator(int creatorId)
        {

            return null;
        }

        /// <summary>
        /// Get Video Details by Video Id
        /// </summary>
        /// <param name="videoId"></param>
        /// <returns></returns>
        public VideoDetails GetVideoDetails(int videoId)
        {

            return null;
        }

        /// <summary>
        /// Get Video Rank by Video Id
        /// </summary>
        /// <param name="videoId"></param>
        /// <returns></returns>
        public Rank GetVideoRank(int videoId)
        {

            return null;
        }

        /// <summary>
        /// Get Video Comments by Video Id
        /// </summary>
        /// <param name="videoId"></param>
        /// <returns></returns>
        public Comments GetVideoComments(int videoId)
        {

            return null;
        }
    }
}
