﻿namespace VideoData
{
    public class Comments
    {
        public int VideoId;
        public int CommentCount;
        public bool CommentsEnabled;
    }
}