﻿using System.Runtime.Serialization;
using ServiceStack;

namespace VideoService
{
    public class ServiceAction : Service
    {
        public class GetServiceAction : Service
        {
            public object Get(ServiceGetRequest request)
            {
                // Force response to JSON format
                Request.ResponseContentType = MimeTypes.Json;
                
                // Get Dependency data for specified namespace
                var result = $"VideoService({request.Value})";

                var response = new ServiceGetResponse { Result = result };
                return response;
            }
        }
    }

    [DataContract]
    [Route("/service/{Value}/", "GET")]
    public class ServiceGetRequest
    {
        [DataMember]
        public string Value { get; set; }
    }

    [DataContract]
    public class ServiceGetResponse
    {
        [DataMember]
        public string Result { get; set; }

    }
}