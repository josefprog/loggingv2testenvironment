﻿using System.Net;
using ServiceStack;
using ServiceStack.Text;

namespace VideoService
{
    public class AppHost : AppHostBase
    {
        public AppHost() : base("VideoService", typeof(ServiceAction).Assembly) { }

        public override void Configure(Funq.Container container)
        {
            CustomErrorHttpHandlers.Remove(HttpStatusCode.Forbidden);
            JsConfig.EmitCamelCaseNames = true;
            

            // Routes.Add<DependencyRequest>("/dependency", ApplyTo.All);
        }
    }
}