﻿using System.Net;
using ServiceStack;
using ServiceStack.Text;

namespace RankService
{
    public class AppHost : AppHostBase
    {
        public AppHost() : base("RankService", typeof(ServiceAction).Assembly) { }

        public override void Configure(Funq.Container container)
        {
            CustomErrorHttpHandlers.Remove(HttpStatusCode.Forbidden);
            JsConfig.EmitCamelCaseNames = true;

            // Routes.Add<DependencyRequest>("/dependency", ApplyTo.All);
        }
    }
}